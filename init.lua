--卐

DISABLE_SKY_ISLANDS=true

local SW_RADIUS = 3
local SW_SPACING = 1
local SW_ROFF = 2

local function swastika_arm(sw,ra,dirs,dirt)
	local x,y=ra+1,ra+1
	for n=1,ra do
		x,y=x+dirs[1],y+dirs[2]
		sw[y][x]=true
	end
	for n=1,ra do
		x,y=x+dirt[1],y+dirt[2]
		sw[y][x]=true
	end
end

local function genswastika(ra)
	local sw={}
	local w=1+ra*2
	for y=1,w do
		sw[y]={}
		for x=1,w do
			sw[y][x]=false
		end
	end
	sw[ra+1][ra+1]=true
	swastika_arm(sw,ra,{0,-1},{1,0})
	swastika_arm(sw,ra,{1,0},{0,1})
	swastika_arm(sw,ra,{0,1},{-1,0})
	swastika_arm(sw,ra,{-1,0},{0,-1})
	return sw,w
end

local swastika,SW_WIDTH=genswastika(SW_RADIUS)
local SW_CELL = SW_WIDTH + SW_SPACING + SW_ROFF

do
	local sw,w=genswastika(5)
	print("\n   SWASTIKA MAPGEN    ")
	for y=1,w do
		for x=1,w do
			io.write(sw[y][x] and "()" or "..")
		end
		io.write("\n")
	end
	print()
	io.flush()
end

local noise,blocks,roffnoise
local function get_roff(x,y)
	return
		math.floor(roffnoise:get_3d({x=x,y=y,z=-10})*SW_ROFF+0.5),
		math.floor(roffnoise:get_3d({x=x,y=y,z=10})*SW_ROFF+0.5)
end
minetest.after(0,function()
	blocks={
		swast=minetest.get_content_id("default:permafrost"),
		swast_top=minetest.get_content_id("default:permafrost_with_moss"),
		floor=minetest.get_content_id("default:cloud")
	}
	noise=minetest.get_perlin({
		seed=2153,
		octaves=8,
		spread={x=500,y=500,z=500},
		lacunarity=2,
		persistence=0.5,
		scale=10,
	})
	roffnoise=minetest.get_perlin({
		seed=25932,
		octaves=1,
		spread={x=1,y=1,z=1},
		lacunarity=1,
		persistence=1,
		scale=1,
		flags={defaults=true,absvalue=true}
	})
end)

local math_floor,math_random=math.floor,math.random
local mapperlin
minetest.after(0, function() mapperlin = minetest.get_perlin(0, 1, 0, 1) end)

local function seeded_rng(seed)
        if PcgRandom then
                seed = math_floor((seed - math_floor(seed)) * 2 ^ 32 - 2 ^ 31)
                local pcg = PcgRandom(seed)
                return function(a, b)
                        if b then
                                return pcg:next(a, b)
                        elseif a then
                                return pcg:next(1, a)
                        end
                        return (pcg:next() + 2 ^ 31) / 2 ^ 32
                end
        end
        return math_random
end


minetest.register_on_generated(function(mi,ma)
	local rng = seeded_rng(mapperlin:get_3d(mi))
	local vm,emi,ema=minetest.get_mapgen_object("voxelmanip")
	local ar=VoxelArea:new{MinEdge = emi,MaxEdge=ema}
	local data=vm:get_data()
	local trees={}
	for x=mi.x,ma.x do for z=mi.z,ma.z do
		local is_sw,miny
		local ox,oy=x+SW_RADIUS+math.floor(SW_ROFF/2+0.5),z+SW_RADIUS+math.floor(SW_ROFF/2+0.5)
		local cx,cy=math.floor(ox/SW_CELL)*SW_CELL,math.floor(oy/SW_CELL)*SW_CELL
		local rx,ry=get_roff(cx/SW_CELL,cy/SW_CELL)
		local central=false
		if cx==0 and cy==0 then
			central=true
			rx,ry=0,0
		end
		ox,oy=ox-rx,oy-ry
		miny=noise:get_2d({x=cx,y=cy})
		local dtz=(cx*cx+cy*cy)^0.5
		dtz=dtz-25
		dtz=dtz/50
		dtz=math.max(0,math.min(1,dtz))
		miny=miny*(dtz)+0*(1-dtz)
		miny=math.floor(miny+0.5)
		local sx,sy=ox%SW_CELL,oy%SW_CELL
		is_sw=false
		local ww=central and SW_CELL-SW_SPACING or SW_WIDTH
		if sx>=0 and sy>=0 and sx<ww and sy<ww then
			local sx,sy=sx,sy
			--if not central then
				sx,sy=sx+1,sy+1
			--end
			is_sw=(cx==0 and cy==0) and true or swastika[sy][(SW_WIDTH-sx+1)]
		end
		for y=mi.y,ma.y do
			local i = ar:index(x,y,z)
			if (y<=miny and is_sw) then
				data[i]=blocks.swast
				if y==miny then
					data[i]=blocks.swast_top
					if rng(10000)==1 then
						table.insert(trees,{x=x,y=y,z=z})
					end
				end
			elseif (y<=-100) then
				data[i]=blocks.floor
			end
		end
	end end
	vm:set_data(data)
	vm:calc_lighting()
	vm:write_to_map()
	for k,v in pairs(trees) do
                minetest.set_node(v,{name="default:dirt"})
                local spos=vector.add(v,vector.new(0,1,0))
                minetest.after(0,default.grow_new_apple_tree,spos)
	end
end)
